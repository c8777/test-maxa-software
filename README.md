# MAXA test
Проект "MAXA test" представляет собой приложение для работы с текстом, фигурами и картинками.
Приложение предоставляет пользователю возможность вставлять текст, различные фигуры и картинки, 
а также производить над ними различные манипуляции: 
изменять размер, вращать, перемещать по рабочей области и т.д.

## Содержание
1. [Что внутри?](#inside)
2. [Начало работы](#get-started)
- [Установка зависимостей](#install-dependencies)
- [Запуск проекта](#run-project)
- [Сборка проекта](#build-project)
- [Ссылка на репозиторий](#link-repository)
- [Ссылка на проект](#link-project)

## <a name="inside">Что внутри?</a>
- [React](https://ru.legacy.reactjs.org/) фреймворк `v18.2.0`
- [typescript](https://www.typescriptlang.org/)`v4.9.5`
- [fabric](http://fabricjs.com/) библиотека холста js HTML5 `v5.3.0`
- [fabricjs-react](https://www.npmjs.com/package/fabricjs-react) библиотека fabric js для react `v1.2.1`
- [fabric-history](https://www.npmjs.com/package/fabric-history) для реализации истории изменений `v1.7.0`
- [@types/fabric](https://www.npmjs.com/package/@types/fabric) пакет содержит определения типов fabric `v5.3.6`
- [sass](https://sass-lang.com/) препроцессор CSS для работы со стилями `v1.69.5`
- [antd](https://ant.design/) библиотека компонентов пользовательского интерфейса `v5.11.2`
- [react-icons](https://react-icons.github.io/react-icons/) для использования react-иконок `v4.12.0`
- Dev зависимости:
  - [@craco/craco](https://www.npmjs.com/package/@craco/craco) библиотека для сборки и запуска проекта. Используется для реализации alias (короткая ссылка "@") `v7.1.0`
  - [prettier](https://www.npmjs.com/package/prettier) для форматирования кода `v3.1.0`
  - [@trivago/prettier-plugin-sort-imports](https://www.npmjs.com/package/@trivago/prettier-plugin-sort-imports) для сортировки импортов `v4.3.0`

## <a name="get-started">Начало работы</a>
### <a name="install-dependencies">Установка зависимостей</a>
Для инициализации проекта устанавливаем зависимости

`npm install`

### <a name="run-project">Запуск проекта</a>
Для запуска проекта необходимо вызвать команду

`npm run start`

### <a name="build-project">Сборка проекта</a>
Для сборки проекта необходимо вызвать команду

`npm run build`

### <a name="link-repository">Ссылка на репозиторий</a>
<https://gitlab.com/c8777/test-maxa-software>
### <a name="link-project">Ссылка на проект</a>
<http://u2360905.isp.regruhosting.ru/>