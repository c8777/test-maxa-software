import { Modal } from 'antd';
import { createContext, useContext, useState } from 'react';

import { useEditor } from '@/context/EditorContext';

export const ModalContext = createContext<{
  openModal: () => void;
}>({
  openModal: () => {}
});

export const useModal = () => useContext(ModalContext);

export const ModalContextProvider = ({ children }: any) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isCopied, setIsCopied] = useState(false);

  const { editor } = useEditor();

  const canvasSvg = editor?.canvas.toSVG() ?? '';

  const handleOk = () => {
    navigator.clipboard.writeText(canvasSvg);
    setIsCopied(true);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
    setIsCopied(false);
  };

  const handleClickModal = () => {
    setIsModalOpen(true);
  };

  return (
    <ModalContext.Provider value={{ openModal: handleClickModal }}>
      <Modal
        title="Save svg"
        open={isModalOpen}
        okText={isCopied ? 'Copied!' : 'Copy'}
        onOk={handleOk}
        cancelText="Close"
        onCancel={handleCancel}>
        <p> {canvasSvg} </p>
      </Modal>
      {children}
    </ModalContext.Provider>
  );
};
