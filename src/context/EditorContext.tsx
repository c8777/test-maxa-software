import { useFabricJSEditor } from 'fabricjs-react';
import { FabricJSEditorHook } from 'fabricjs-react/dist/lib/editor';
import { createContext, useContext, useEffect, useState } from 'react';

import { CanvasWithHistory } from '@/types';

type EditorContextType = FabricJSEditorHook;

export const EditorContext = createContext<EditorContextType>({ onReady: () => {} });

export const useEditor = () => useContext(EditorContext);

export const EditorContextProvider = ({ children }: any) => {
  const editor = useFabricJSEditor();
  const [isTextEditing, setIsTextEditing] = useState<boolean>(false);

  useEffect(() => {
    editor.editor?.canvas.on('text:editing:entered', handleTextEditingEntered);
    editor.editor?.canvas.on('text:editing:exited', handleTextEditingExited);

    window.addEventListener('keydown', handleKeyDown);

    return () => {
      editor.editor?.canvas.off('text:editing:entered', handleTextEditingEntered);
      editor.editor?.canvas.off('text:editing:exited', handleTextEditingExited);
      window.removeEventListener('keydown', handleKeyDown);
    };
  }, [editor]);

  const handleKeyDown = (event: KeyboardEvent) => {
    if (isTextEditing) return;

    if (event.key === 'Backspace' || event.key === 'Delete') {
      editor.editor?.deleteSelected();
    }

    if (event.ctrlKey && event.code === 'KeyZ' && editor.editor) {
      (editor.editor.canvas as CanvasWithHistory).undo();
    }

    if (event.ctrlKey && event.code === 'KeyY' && editor.editor) {
      (editor.editor.canvas as CanvasWithHistory).redo();
    }
  };

  const handleTextEditingEntered = () => {
    setIsTextEditing(true);
  };

  const handleTextEditingExited = () => {
    setIsTextEditing(false);
  };

  return <EditorContext.Provider value={editor}>{children}</EditorContext.Provider>;
};
