import { ConfigProvider } from 'antd';
import { fabric } from 'fabric';
import 'fabric-history';
import React from 'react';

import { EditorCanvas } from '@/components/EditorCanvas/EditorCanvas';
import { EditorContextProvider } from '@/context/EditorContext';

import { PageLayout } from './components/PageLayout/PageLayout';
import './styles/global.scss';

window.fabric = fabric;

function App() {
  return (
    <EditorContextProvider>
      <ConfigProvider
        theme={{
          token: {
            colorPrimary: '#AC62D9'
          },
          components: {
            Layout: {
              siderBg: 'transparent',
              bodyBg: 'transparent'
            },
            Menu: {
              darkItemBg: 'transparent',
              darkSubMenuItemBg: '#65666999',
              darkItemSelectedBg: 'none'
            }
          }
        }}>
        <div className="App">
          <PageLayout>
            <EditorCanvas />
          </PageLayout>
        </div>
      </ConfigProvider>
    </EditorContextProvider>
  );
}

export default App;
