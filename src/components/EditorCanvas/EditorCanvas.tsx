import { FabricJSCanvas } from 'fabricjs-react';

import { useEditor } from '@/context/EditorContext';

import style from './style.module.scss';

export const EditorCanvas = () => {
  const { onReady } = useEditor();

  return (
    <div className={style.editorCanvas}>
      <FabricJSCanvas className={style.canvas} onReady={onReady} />
    </div>
  );
};
