import { Button, Layout } from 'antd';
import { useState } from 'react';
import { FaAngleLeft } from 'react-icons/fa6';
import { FaAngleRight } from 'react-icons/fa6';

import { MenuList } from '@/components/PageLayout/components/Sidebar/components/MenuList';

import style from './style.module.scss';

const { Sider } = Layout;

export const Sidebar = () => {
  const [isCollapsed, setColladsed] = useState(true);

  const handleClickShowMoreActive = (): void => {
    setColladsed(!isCollapsed);
  };

  return (
    <div className={style.container}>
      <Layout>
        <Sider className={style.sidebar} collapsed={isCollapsed} collapsedWidth={40}>
          <Button
            type="text"
            className={style.toggle}
            onClick={handleClickShowMoreActive}
            icon={isCollapsed ? <FaAngleRight color="white" /> : <FaAngleLeft color="white" />}
          />
          <MenuList />
        </Sider>
      </Layout>
    </div>
  );
};
