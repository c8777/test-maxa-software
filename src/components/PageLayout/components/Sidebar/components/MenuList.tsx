import { UploadOutlined } from '@ant-design/icons';
import { Menu } from 'antd';
import { fabric } from 'fabric';
import React, { ChangeEventHandler } from 'react';
import { FaRegSquare } from 'react-icons/fa';
import { FaRegCircle } from 'react-icons/fa';
import { IoImageOutline } from 'react-icons/io5';
import { RxText } from 'react-icons/rx';
import { TbTriangleSquareCircle } from 'react-icons/tb';
import { TbLine } from 'react-icons/tb';

import { useEditor } from '@/context/EditorContext';

import style from './style.module.scss';

export const MenuList = () => {
  const { editor } = useEditor();

  const handleAddText = () => {
    editor?.addText('Текст');
  };

  const handleAddRectangle = () => {
    editor?.addRectangle();
  };

  const handleAddCircle = () => {
    editor?.addCircle();
  };

  const handleAddLine = () => {
    editor?.addLine();
  };

  const handleUploadImage: ChangeEventHandler<HTMLInputElement> = (event) => {
    const file = event.target.files?.[0];

    if (!file) return;

    const fileURL = URL.createObjectURL(file);
    fabric.Image.fromURL(fileURL, function (oImg) {
      editor?.canvas.add(oImg);
    });
  };

  return (
    <Menu theme="dark" className={style.menuList}>
      <Menu.Item className={style.menuItem} key="text" onClick={handleAddText} icon={<RxText />}>
        Text
      </Menu.Item>
      <Menu.SubMenu
        className={style.menuItem}
        key="figures"
        icon={<TbTriangleSquareCircle />}
        title="Figures">
        <Menu.Item
          className={style.menuItem}
          key="square"
          onClick={handleAddRectangle}
          icon={<FaRegSquare />}>
          Rectangle
        </Menu.Item>
        <Menu.Item
          className={style.menuItem}
          key="circle"
          onClick={handleAddCircle}
          icon={<FaRegCircle />}>
          Circle
        </Menu.Item>
        <Menu.Item className={style.menuItem} key="line" onClick={handleAddLine} icon={<TbLine />}>
          Line
        </Menu.Item>
      </Menu.SubMenu>

      <Menu.SubMenu className={style.menuItem} key="image" icon={<IoImageOutline />} title="Image">
        <Menu.Item className={style.menuItem} key="upload" icon={<UploadOutlined />}>
          Upload png/jpg..
          <input
            type="file"
            className={style.importFileButton}
            onChange={handleUploadImage}
            accept=".jpg,.jpeg,.png"
          />
        </Menu.Item>
      </Menu.SubMenu>
    </Menu>
  );
};
