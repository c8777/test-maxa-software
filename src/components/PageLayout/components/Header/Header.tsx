import { LuUndo } from 'react-icons/lu';
import { LuRedo } from 'react-icons/lu';
import { PiMagnifyingGlassMinusBold } from 'react-icons/pi';
import { PiMagnifyingGlassPlusBold } from 'react-icons/pi';

import logo from '@/assets/logo/logo-maxa-test.svg';
import { useEditor } from '@/context/EditorContext';
import { useModal } from '@/context/ModalContext';
import { CanvasWithHistory } from '@/types';

import style from './style.module.scss';

export const Header = () => {
  const { editor } = useEditor();
  const { openModal } = useModal();

  const handleClickZoomOut = () => {
    editor?.zoomOut();
  };
  const handleClickZoomIn = () => {
    editor?.zoomIn();
  };

  const handleClickClearCanvas = () => {
    editor?.deleteAll();
  };

  const handleClickSave = () => {
    openModal();
  };

  const handleClickUndo = () => {
    if (!editor) return;

    (editor.canvas as CanvasWithHistory).undo();
  };

  const handleClickRedo = () => {
    if (!editor) return;

    (editor.canvas as CanvasWithHistory).redo();
  };

  return (
    <div className={style.header}>
      <img src={logo} alt="Logo" className={style.logo} />

      <div className={style.viewButtons}>
        <div className={style.zoom}>
          <PiMagnifyingGlassMinusBold className={style.zoomButton} onClick={handleClickZoomOut} />
          <PiMagnifyingGlassPlusBold className={style.zoomButton} onClick={handleClickZoomIn} />
        </div>
      </div>

      <div className={style.actions}>
        <button className={style.clear} onClick={handleClickClearCanvas}>
          Clear all
        </button>

        <div className={style.historyButtons}>
          <LuUndo className={style.historyIcon} onClick={handleClickUndo} title="CTRL + Z" />
          <LuRedo className={style.historyIcon} onClick={handleClickRedo} title="CTRL + Y" />
        </div>

        <button className={style.save} onClick={handleClickSave}>
          Save
        </button>
      </div>
    </div>
  );
};
