import { PropsWithChildren } from 'react';

import { Sidebar } from '@/components/PageLayout/components/Sidebar/Sidebar';
import { ModalContextProvider } from '@/context/ModalContext';

import { Header } from './components/Header/Header';
import style from './style.module.scss';

type Props = PropsWithChildren;

export const PageLayout = ({ children }: Props) => {
  return (
    <div className={style.pageLayout}>
      <ModalContextProvider>
        <Header />
      </ModalContextProvider>
      <Sidebar />
      <div className={style.content}>{children}</div>
    </div>
  );
};
