import { Canvas } from 'fabric/fabric-impl';

export type CanvasWithHistory = Canvas & {
  undo: () => void;
  redo: () => void;
};
